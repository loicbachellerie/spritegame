class Background {
	constructor() {
		this.image = new Image();
		this.image.src = "images/bg.jpg";
	}

	tick() {
		let alive = true;

		if (this.image.complete) {
			ctx.drawImage(this.image, 0, 0);
		}

		return alive;
	}
}