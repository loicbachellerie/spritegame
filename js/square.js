class Square {
    
        constructor(x, y) {
            this.x = x;
            this.y = y;
        }
    
        tick() {
        
            ctx.fillStyle = "rgb(150, 150, 150)";
            ctx.fillRect(this.x, this.y);
        }
    }