let spriteList = [];
let ctx = null;
let mouse = {x : -1, y : -1};

// On attend le chargement de la page
window.onload = () => {
	ctx = document.getElementById("canvas").getContext("2d");

	// On va chercher les coord. de la souris (et mettre dans une variable)
	document.getElementById("canvas").onmousemove = e => {
		mouse = {
			x : e.offsetX,
			y : e.offsetY
		}
	}

	// Lorsque l'on clique sur le bouton...
	document.getElementById("btn-add").onclick = () => {
		let x = parseInt(document.getElementById("x").value);
		let y = parseInt(document.getElementById("y").value);
		let size = parseInt(document.getElementById("size").value);

		// Ajout d'un nouveau carré
		spriteList.push(new Square(x, y, size));
	}

	mainTick(); 
}

// Se rappelle toujours, et automatiquement! (boucle d'animation)
function mainTick () {
	for (let i = 0; i < spriteList.length; i++) {
		const element = spriteList[i];
		element.tick();
	}
	
	window.requestAnimationFrame(mainTick);
}