class Player{
	constructor(x=null, y=null, id=null){
		if(x == null && y == null && id == null){
			this.x = Math.random() * 1280;
			this.y = Math.random() * 720;
			this.id = Math.round(window.performance.now()).toString();
			this.updateInDatabase();
		}
		else{
			this.x = x;
			this.y = y;
			this.id = id;
		}
		
		firebase.database().ref().child("players").child(this.id).onDisconnect().set(null);

		this.speedX = 0;
		this.speedY = 0;
	}

	setPosition(position){
		this.x = position.x;
		this.y = position.y;
	}
	
	tick(){
		let alive = true;
		if(currentPlayer == this){
			this.move();
			this.updateInDatabase();
		}
		return alive;
	}
	move(){
		this.x += this.speedX;
		this.y += this.speedY;
	}
	updateInDatabase(){
		firebase.database().ref().child("players").child(this.id).set({x:this.x, y:this.y});
	}
	draw(){
		let size = 50;
		ctx.fillStyle = "black";
		ctx.fillRect(this.x-size/2, this.y-size/2, size, size);
	}
}