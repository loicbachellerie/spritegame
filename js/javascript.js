let playersReference;//firebase référence
let dbRef = firebase.database().ref().child('players');//root of the players "table"

let currentPlayer;//joueur actuel
let players = {};//joueurs en jeu

let canvas;
let ctx;



window.onload = () => {
	
	canvas = document.getElementById("canvas");
	ctx = canvas.getContext("2d");

	//current player
	currentPlayer = new Player();
	players[currentPlayer.id] = currentPlayer;

	//retreive all players
  	dbRef.once('value', function(snapshot) {
    	snapshot.forEach(function(childSnapshot) {
			let childData = childSnapshot.val();
			players[childSnapshot.key] = new Player(childData.x, childData.y, childSnapshot.key);//add the player to the array
		});
	  });
	  setupPlayerPositionUpdates();
	  mainTick();
}

function setupPlayerPositionUpdates(){
	dbRef.on('value', function(allChildrenSnapshot){
		allChildrenSnapshot.forEach(function(currentChildSnapshot){
			let childKey = currentChildSnapshot.key;
			let childData = currentChildSnapshot.val();
			players[childKey].setPosition(childData)
		});
	});
	dbRef.on('child_added', function(allChildrenSnapshot){
		allChildrenSnapshot.forEach(function(currentChildSnapshot){
			let childKey = currentChildSnapshot.key;
			let childData = currentChildSnapshot.val();

			players[childKey] = new Player(childData.x, childData.y, childData.id);
		});
	});
}

function mainTick(){
	ctx.clearRect(0,0,canvas.width,canvas.height);
	for(let player in players){
		//console.log(players[player])
		players[player].tick();
	}
	for(let player in players){
		players[player].draw();
	}
	window.requestAnimationFrame(mainTick);
}
