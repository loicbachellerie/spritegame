var markerTab = {};
var mapTab = {};
var currentBigMap;
var currentBigMapMarker;
var currentViewedId;
var messaging;

var config = {
    apiKey: "AIzaSyDoLdMrSxkVk7-lzBU0wIzpRMxLTir0tjo",
    authDomain: "lifedata-24e9d.firebaseapp.com",
    databaseURL: "https://lifedata-24e9d.firebaseio.com",
    projectId: "lifedata-24e9d",
    storageBucket: "lifedata-24e9d.appspot.com",
    messagingSenderId: "1054473395165"
};

window.onload = () => {
  firebase.initializeApp(config);
  loadInitialData();
  setupViewUpdates();
  messaging = firebase.messaging();
  //var dbRef = firebase.database().ref().child('name');
  //dbRef.on('value', snap => valeur.innerText = snap.val());
};

function loadInitialData(){
  var valeur = document.getElementById('operator-view');
  var dbRef = firebase.database().ref().child('alerts');
  dbRef.once('value', function(snapshot) {
    valeur.innerHTML = "";
    snapshot.forEach(function(childSnapshot) {genererDivAvecMap(childSnapshot)});
  });
}


function setupViewUpdates(){
  // retrieve the last record from `ref`
  var ref = firebase.database().ref().child('alerts');
  ref.on('child_added', function(addedChild) {genererDivAvecMap(addedChild)});
  ref.on('child_removed', function(removedChild){
    removedDiv = document.getElementById(removedChild.key+"-container");
    removedDiv.parentElement.removeChild(removedDiv);
  })
  ref.on('value', function(allChildren){
    allChildren.forEach(function(currentChild){
     var childData = currentChild.val();
     var childKey = currentChild.key;
      var lat = childData.latitude;
      var lng = childData.longitude;
      mapTab[childKey].setCenter({lat:lat, lng:lng, alt:0});
      markerTab[childKey].setPosition({lat:lat, lng:lng, alt:0});
      
      document.getElementById(childKey+"-heart-rate").innerText = childData.heartrate;
      if(currentViewedId == childKey){
        currentBigMap.setCenter({lat:lat, lng:lng, alt:0});
        currentBigMapMarker.setPosition({lat:lat, lng:lng, alt:0});
      }
    });
  });
}


function genererDivAvecMap(entrysnapshot){
  let childKey = entrysnapshot.key;
  let childData = entrysnapshot.val();
  $("#operator-view").append(genererDivVide(childKey, childData));
  
  // modifie le modal 
  $(".buttonModal").hover(function() {
    var id = $(this).attr('data-target');
    var res = id.substring(1, id.lenght);
    $("div.modal.fade").attr("id", res);
    genererModal(res.replace('-modal',''));
   });
  
  var position = {lat: childData.latitude, lng: childData.longitude};
  //let map = new google.maps.Map(document.getElementById(childKey), {
  mapTab[childKey] = new google.maps.Map(document.getElementById(childKey), {
      zoom: 8,
      center: position
    });
  markerTab[childKey] = new google.maps.Marker({
      position: position,
      map: mapTab[childKey]
    });
  
  var heartrate = childData.heartrate;
  if(heartrate != null){
    document.getElementById(childKey+"-heart-rate").innerText = heartrate;
  }
  else{
    document.getElementById(childKey+"-heart-rate").innerText = "N/A";
  }
  
}

function genererModal(id){
  currentViewedId = id;
  var modalTitle = document.getElementById("modalTitle");
  var heartrate = document.getElementById("heartRate");
  var dbRef = firebase.database().ref().child('alerts/'+id);
  
  //INSERER DANS LES DIVS
  modalTitle.innerText = id.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1 $2 $3');
  var position;
  dbRef.on('value', snapshot => {
    heartrate.innerText = snapshot.val().heartrate;
    position = {lat: snapshot.val().latitude, lng: snapshot.val().longitude};
  });
  //let map = new google.maps.Map(document.getElementById(childKey), {
   currentBigMap = new google.maps.Map(document.getElementById('mapModal'), {
      zoom: "12",
      center: position
    });
    currentBigMapMarker = new google.maps.Marker({
      position: position,
      map: currentBigMap
    });
    var center = currentBigMap.getCenter();
    $('#mapModal').on('shown.bs.modal', function () {
      google.maps.event.trigger(maps[0].map, "resize");
    });
    //google.maps.trigger(currentBigMap, "resize");
    currentBigMap.setCenter(center);
    currentBigMap.setZoom(12);
    currentBigMap.setCenter(position);
    //currentBigMapMarker.setPosition(position);
}
function post() {
  var instanceid = firebase.database().ref().child('alerts').child(currentViewedId).child("instanceId");
  var content = JSON.stringify({
      "to": instanceid, //browser token here
      "data": {
        "notification": {
          "body": "help is on the way",
          "title": "help is on the way"
        }
      },
      "priority": 10
  });
      $.ajax({
          type : 'POST',
          url : "https://fcm.googleapis.com/fcm/send",
          headers : {
              Authorization : 'key=' + config.apiKey
          },
          contentType : 'application/json',
          data : content,
          success : function(response) {
              console.log(response);
          },
          error : function(xhr, status, error) {
              console.log(xhr.error);                   
          }
      });
}
function sendNotification(token){
  
}
function genererDivVide(childKey, childData){
  var phone = childKey.replace(/(\d\d\d)(\d\d\d)(\d\d\d\d)/, '$1 $2 $3');

  var alerte = '<div class="col-lg-3 oneAlert" id="'+childKey+'-container">'
              +'<div class="thumbnail">'
              +'<div id="'+childKey+'" style="height:150px;"></div>'
              +'<div class="caption">'
              +'<h2 class="alertLabel">'+childKey+" - "+phone+'</h2><br>'
              +'<p>Lorem ipsum dolor sit amet,itis repellat.</p><br>'
              +'<div class="heartrateContainer row">'
              +'<div class="col-lg-2"></div>'
              +'<div class="col-lg-4 heartrate-div-parent">'
              +'<i class="heartrate-icon fas fa-heart iconeRouge"></i>'
              +'<div class="col-lg-2 vcenter heartrate-div"><p class="heartrateLabel" id="'+childKey+'-heart-rate"></p></div>'
              +'</div>'
              +'<a href="#" class="col-lg-4 btn btn-info btn-md buttonModal" role="button" data-toggle="modal" data-target="#'+ childKey+'-modal">Ouvrir</a>'
              +'<div class="col-lg-2"></div>'
              +'</div>'
              +'</div></div></div>';
  return alerte;
}